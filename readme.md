# sql-backup

This performs a backup of a sql db to a (timestamped) `bacpac` within a `Storage Account`

```
{storageContainerUrl}/{sqlServerName}/{sqlDatabaseName}/{sqlServerName}-{sqlDatabaseName}-{YYYMMDD}-{HHMMSS}.bacpac

e.g.

https://backupstorage.blob.core.windows.net/bacpacs/sqlserver/db/sqlserver-db-20170228-230410.bacpac
```
## Configuration

Within the `Automation Account` you need to setup:
- 4 items within `Credentials` (all values supplied to parameters with names ending in `CredentialName`)


> This should be scheduled to run at regular intervals

## Parameters

### Required
| param | description | example |
| - | -| - |
| sqlCredentialName | The sql db credential name | `"my-sql-credential"` |
| sqlServerName | The sql server name | `"sqlserver"` |
| sqlDatabaseName | The sql db name | `"db"` |
| resourceGroupName | The resource group of the sql server | `"sql-group"` |
| emailFrom | The email sender | `"""Sql Backup Automation"" <no-reply@my-domain.com>"` |
| emailTo | The email recipient | `["report-to-me@my-domain.com", "report-to-me-also@my-domain.com"]` |


### Optional
| param | description | default |
| - | -| - |
| backupJobName | The Identifier of the backup operation | `"SQL backup"` |
| subscriptionCredentialName | The azure subscription credential name | `"azure-subscription"` |
| storageKeyCredentialName | The key required to access the `Storage Account` container (above) - username is url of container| `"backup-storage-key"` |
| emailCredentialName | The email credential name | `"sendgrid"` |
| emailSmtpServer | The SMTP server | `"smtp.sendgrid.net"` |
| emailPort | The SMTP port | `587` |
| sendEmailOnSuccess | Emails are only sent on failure by default | `false` |


# sql-restore

This restores a timestamped bacpac to the same server as the original with a new db name!

> This restores a backup performed by `sql-backup.ps1`

## Configuration

Within the `Automation Account` you need to setup:
1. 3 items within `Credentials` (all values supplied to parameters with names ending in `CredentialName`)

> This should be scheduled to run once as needed!


## Parameters

### Required
| param | description | example |
| - | -| - |
| sqlCredentialName | The sql db credential name | `"my-sql-credential"` |
| sqlServerName | The sql server name | `"sqlserver"` |
| sqlDatabaseName | The sql db name | `"db"` |
| resourceGroupName | The resource group of the sql server | `"sql-group"` |
| databaseBackupTime | The timestamp of the backup in `YYYMMDD-HHMMSS` format | `"20170228-230410"` |
| sqlCreateDatabaseName | The database to create to restore the bacpac to | `"db-restore"` |


### Optional
| param | description | default |
| - | -| - |
| sqlCreateDatabaseEdition |  "Standard", or "Premium" | `"Standard"` |
| sqlCreateDatabaseServiceObjectiveName | Indicates the performance level of the db to create | `"S2"` |
| sqlCreateDatabaseMaxSizeBytes | The max size of the db | `2147483647` |
| subscriptionCredentialName | The azure subscription credential name | `"azure-subscription"` |
| storageKeyCredentialName | The key required to access the `Storage Account` container (above) - username is url of container | `"backup-storage-key"` |
