param(
    [Parameter(Mandatory=$true)]
    [string] $sqlCredentialName,

    [Parameter(Mandatory=$true)]
    [string] $sqlServerName,

    [Parameter(Mandatory=$true)]
    [string] $sqlDatabaseName,

    [Parameter(Mandatory=$true)]
    [string] $resourceGroupName,

    [Parameter(Mandatory=$true)]
    [string] $databaseBackupTime, # "yyyyMMdd-HHmmss"

    [Parameter(Mandatory=$true)]
    [string] $sqlCreateDatabaseName,

    [Parameter(Mandatory=$false)]
    [string] $sqlCreateDatabaseEdition = "Standard",

    [Parameter(Mandatory=$false)]
    [string] $sqlCreateDatabaseServiceObjectiveName = "S2",

    [Parameter(Mandatory=$false)]
    [int] $sqlCreateDatabaseMaxSizeBytes  = 2147483647,

    [Parameter(Mandatory=$false)]
    [string] $subscriptionCredentialName = "azure-subscription",

    [Parameter(Mandatory=$false)]
    [string] $storageKeyCredentialName = "backup-storage-key",
)

# $ErrorActionPreference = "Continue"

function Write-LogMessage($message)
{
	$timestamp = Get-Date -format "yyyy-MM-dd HH:mm.ss";
	Write-Output "$timestamp :: $message"
}

function Get-Credential ([string] $name, [string] $type) {
    #Write-LogMessage "Getting the $type credential: $name."
    $cred = Get-AutomationPSCredential -Name $name;
    if (!$cred)
    {
        throw New-Object System.Exception "Unable to get the $type credential: $name."
    }
    return $cred
}

function Get-Variable([string] $name, [string] $type){
    #Write-LogMessage "Getting the $type variable: $name."
    $variable = Get-AutomationVariable -Name $name;
    if (!$variable)
    {
        throw New-Object System.Exception "Unable to get the $type variable: $name."
    }
    return $variable
}

function Login(){
    $subCred = Get-Credential $subscriptionCredentialName "Azure Subscription"
    Add-AzureRmAccount -Credential $subCred
}

function Restore-Bacpac([string] $bacpacStorageUrl){
    $sqlCredential = Get-Credential $sqlCredentialName "SQL Server"
    $storageKeyCredential = Get-Credential $storageKeyCredentialName "Storage Key"
    $netCredential = $storageKeyCredential.GetNetworkCredential()
    $storageKey = $netCredential.Password
    $storageContainerUrl = $netCredential.UserName

    Write-LogMessage ("Requesting a SQL Import");
    $sqlRequest =  New-AzureRmSqlDatabaseImport -ResourceGroupName $resourceGroupName -ServerName $sqlServerName  `
    -DatabaseName $sqlCreateDatabaseName -StorageKeyType "StorageAccessKey" -StorageKey $storageKey -StorageUri $bacpacStorageUrl `
    -AdministratorLogin $sqlCredential.UserName -AdministratorLoginPassword $sqlCredential.Password `
    -Edition $sqlCreateDatabaseEdition -ServiceObjectiveName $sqlCreateDatabaseServiceObjectiveName -DatabaseMaxSizeBytes $sqlCreateDatabaseMaxSizeBytes 
    
    if (!$sqlRequest)
    {
        throw New-Object System.Exception "Unable to generate import request for BACPAC $bacpacStorageUrl to $sqlCreateDatabaseName database."
    }
}

try
{
    Write-LogMessage "Restore BACPAC : Starting";    
    Write-LogMessage "Logging in to Azure..." 
    Login
    $databaseCopyName = "$sqlDatabaseName-$databaseBackupTime"
    $bacpacStorageUrl = "$storageContainerUrl/$sqlServerName/$sqlDatabaseName/$sqlServerName-$databaseCopyName.bacpac"

    Write-LogMessage "Restoring database BACPAC $bacpacStorageUrl to $sqlCreateDatabaseName"
    Restore-Bacpac $bacpacStorageUrl
    Write-LogMessage "Restore BACPAC : Finished";
}
catch {                                 
    Write-Error -Message $_.Exception
    throw $_.Exception
}
