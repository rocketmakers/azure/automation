param(
    [Parameter(Mandatory=$true)]
    [string] $sqlCredentialName,

    [Parameter(Mandatory=$true)]
    [string] $sqlServerName,

    [Parameter(Mandatory=$true)]
    [string] $sqlDatabaseName,

    [Parameter(Mandatory=$true)]
    [string[]] $emailTo,

    [Parameter(Mandatory=$true)]
    [string] $resourceGroupName,

    [Parameter(Mandatory=$true)]
    [string] $emailFrom,

    [Parameter(Mandatory=$false)]
    [string] $backupJobName = "SQL backup",

    [Parameter(Mandatory=$false)]
    [string] $subscriptionCredentialName = "azure-subscription",

    [Parameter(Mandatory=$false)]
    [string] $storageKeyCredentialName = "backup-storage-key",

    [Parameter(Mandatory=$false)]
    [string] $emailCredentialName = "sendgrid",

    [Parameter(Mandatory=$false)]
    [string] $emailSmtpServer = "smtp.sendgrid.net",

    [Parameter(Mandatory=$false)]
    [int] $emailPort = 587,

    [Parameter(Mandatory=$false)]
    [boolean] $sendEmailOnSuccess = $false
)

# $ErrorActionPreference = "Continue"

function Write-LogMessage($message)
{
	$timestamp = Get-Date -format "yyyy-MM-dd HH:mm.ss";
	Write-Output "$timestamp :: $message"
}

function Get-Credential ([string] $name, [string] $type) {
    #Write-LogMessage "Getting the $type credential: $name."
    $cred = Get-AutomationPSCredential -Name $name;
    if (!$cred)
    {
        throw New-Object System.Exception "Unable to get the $type credential: $name."
    }
    return $cred
}

function Send-MailMsg ([string] $body) {
    $emailCredential = Get-Credential $emailCredentialName "Email"
    Write-LogMessage "Sending Failure Message: $body"
    Send-MailMessage -To $emailTo -Subject "$backupJobName failure"  -Body $body -UseSsl -Port $emailPort  -SmtpServer $emailSmtpServer  -From $emailFrom -BodyAsHtml -Credential $emailCredential 
}

function Login(){
    $subCred = Get-Credential $subscriptionCredentialName "Azure Subscription"
    Add-AzureRmAccount -Credential $subCred
}

function Start-DbCopy([string] $databaseCopyName){
    $copy = New-AzureRmSqlDatabaseCopy -ResourceGroupName $resourceGroupName -ServerName $sqlServerName -DatabaseName $sqlDatabaseName -CopyServerName $sqlServerName -CopyDatabaseName $databaseCopyName
    if (!$copy)
    {
        throw New-Object System.Exception "Unable to copy $sqlDatabaseName database to $databaseCopyName."
    }
}

function Export-DbCopy([string] $databaseCopyName){
    $sqlCredential = Get-Credential $sqlCredentialName "SQL Server"
    $storageKeyCredential = Get-Credential $storageKeyCredentialName "Storage Key"
    $netCredential = $storageKeyCredential.GetNetworkCredential()
    $storageKey = $netCredential.Password
    $storageContainerUrl = $netCredential.UserName
    $bacpacStorageUrl = "$storageContainerUrl/$sqlServerName/$sqlDatabaseName/$sqlServerName-$databaseCopyName.bacpac"

    $sqlRequest = New-AzureRmSqlDatabaseExport -ResourceGroupName $resourceGroupName -ServerName $sqlServerName `
    -DatabaseName $databaseCopyName -StorageKeytype "StorageAccessKey" -StorageKey $storageKey -StorageUri $bacpacStorageUrl `
    -AdministratorLogin $sqlCredential.UserName -AdministratorLoginPassword $sqlCredential.Password
    if (!$sqlRequest)
    {
        throw New-Object System.Exception "Unable to generate export request for $databaseCopyName database."
    }
    $exportStatus = $sqlRequest.Status
    while ($exportStatus -eq "InProgress" ) {
        Write-LogMessage "Waiting for SQL Database Export to complete"
        Start-Sleep -s 10
        $exportStatus = (Get-AzureRmSqlDatabaseImportExportStatus -OperationStatusLink $sqlRequest.OperationStatusLink).Status
    }

    Write-LogMessage "SQL Export finished with status $exportStatus"
    if ($exportStatus -ne "Succeeded"){
        throw New-Object System.Exception "SQL Database Export did not Succeed: Final Status $exportStatus"        
    }
}

function Remove-DbCopy([string] $databaseCopyName) {
    Remove-AzureRmSqlDatabase -ResourceGroupName $resourceGroupName -ServerName $sqlServerName -DatabaseName $databaseCopyName
}

try
{
    Write-LogMessage "$backupJobName : Starting";    
    $databaseBackupTime = (Get-Date).ToString("yyyyMMdd-HHmmss")
    $databaseCopyName = "$sqlDatabaseName-$databaseBackupTime"
    Write-LogMessage "Logging in to Azure..." 
    Login
    Write-LogMessage "Copying database $sqlDatabaseName to $databaseCopyName"
    Start-DbCopy $databaseCopyName
    try
    {
        Write-LogMessage "Requesting an Export of the database $databaseCopyName"
        Export-DbCopy $databaseCopyName
    }
    catch {
        $ex = $_.Exception                   
        Write-Error -Message $ex
        throw $ex
    } 
    finally {
        Write-LogMessage "Removing database $databaseCopyName"
        Remove-DbCopy $databaseCopyName
    }
    Write-LogMessage "$backupJobName : Finished";
    if ($sendEmailOnSuccess){
        Send-MailMsg "Backup success!" 
    }
}
catch {                                 
    Write-Error -Message $_.Exception
    Send-MailMsg $_.Exception.Message
    throw $_.Exception
}
